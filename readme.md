## 请切换到 development 分支进行下一步

## composer install

执行该命令后：

1. 项目根目录下会新建一个 vendor 文件夹
2. 命令会将 composer.json 写的一些 lib 下载到 vendor 文件夹下

## composer dump-autoload

- 加载 app/helpers.php；
- 见 composer.json 的 autoload。

## cp .env.example .env

复制 .env.example 并新建一个 .env 文件

## php artisan key:generate

- 执行该命令会输出：Application key [XXXXXX] set successfully.

## 修改 .env 文件的 APP_KEY

- laravel 会自动填入 APP_KEY 字段，如果没有：
- 复制上一个命令得到的 key 中括号里面的字符串，粘贴到 .env 文件的 APP_KEY 等于号后面

## 修改 .env 数据库配置

- DB_CONNECTION 为你所使用的数据库类别，该项目使用 mysql，无需更改
- DB_HOST 为数据库访问的 IP，对于开发者，设置为 localhost 即可，无需更改
- DB_PORT 为数据库的访问端口，mysql 一般为 3306，无需更改
- DB_DATABASE 为该项目使用的数据库名称，我们定义为 coolcode，如果本地数据无则新建一个 coolcode 数据库
- DB_USERNAME 为数据库访问用户名，设置为你本地的数据库访问用户名即可
- DB_PASSWORD 为数据库用户的访问密码，设置为你本地的数据库访问用户密码即可
- DB_CHARSET 数据库字符集
- DB_COLLATION 用于指定数据集如何排序，以及字符串的比对规则
- DB_PREFIX 数据库表前缀

注意：

- 新建数据库的编码请使用 utf8mb4

## 修改 .env 测试URL和账户密码配置
- TEST_URL 是你准备测试的项目的 api 地址。如：http://my-coolcode-project.test/ 或线上测试环境的 api 地址
- TEST_EMAIL 用来测试的账号，本地请自行准备；线上请向有关人员索取
- TEST_PASSWORD 用来测试的账号密码，本地请自行准备；线上请向有关人员索取

## 建议
- 为测试需要经常使用的命令设置 alias，如
```
p=phpunit
pf='p --filter'
phpunit=vendor/phpunit/phpunit/phpunit
```
- 
## 验证项目是否运行成功
- 在项目文件夹下运行 `vendor/phpunit/phpunit/phpunit --filter LoginTest`，出现 OK 则运行成功
