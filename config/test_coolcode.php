<?php

return [
  'email'    => env('TEST_EMAIL', ''),
  'mobile'    => env('TEST_MOBILE', ''),
  'password' => env('TEST_PASSWORD', ''),
  'url'      => env('TEST_URL', ''),
];