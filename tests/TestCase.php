<?php namespace Tests;

use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Traits\JsonAssertionsTrait;
use Tests\Traits\ResponseHelpersTrait;

abstract class TestCase extends BaseTestCase {
  use CreatesApplication;
  use ResponseHelpersTrait;
  use JsonAssertionsTrait;

  protected $headers;
  protected $user;
  protected $mobile;
  protected $email;
  protected $password;
  protected $token;
  protected $response;

  /**
   * @var \GuzzleHttp\Client
   */
  protected $http;

  /**
   * @return Client
   */
  protected function up()
  {
    $this->http = new Client([
      'base_uri'    => config('test_coolcode.url'),
      'timeout'     => 20.0,
      'http_errors' => false,
    ]);

    return $this->http;
  }

  /**
   * @return Client
   */
  protected function signIn()
  {
    $this->mobile = config('test_coolcode.mobile');
    $this->password = config('test_coolcode.password');

    $this->up();

    $response = $this->http->post('/api/auth/login/mobile', [
      'form_params' => [
        'mobile'    => $this->mobile,
        'password' => $this->password,
      ],
    ]);

    $this->token = json_decode($response->getBody(), true)['token'];

    return $this->http;
  }

  protected function signOut()
  {
    //todo: 注销
  }

  /**
   * @param array $params
   * @return array
   */
  protected function getHeaders($params = [])
  {
    if (!array_key_exists('Authorization', $params)) {
      $params['Authorization'] = 'Bearer ' . $this->token;
    }

    return $params;
  }

  /**
   * @param array $query
   * @return array
   */
  protected function getQuery($query = [])
  {
    if (!array_key_exists('page_size', $query)) {
      $query['page_size'] = '10';
    }
    if (!array_key_exists('page', $query)) {
      $query['page'] = '1';
    }
    if (!array_key_exists('keyword', $query)) {
      $query['keyword'] = '';
    }
    if (!array_key_exists('sort', $query)) {
      $query['sort'] = '';
    }

    return $query;
  }

}
