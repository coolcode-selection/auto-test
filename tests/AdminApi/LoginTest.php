<?php namespace Tests\AdminApi;

use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Tests\TestCase;

class LoginTest extends TestCase {

  /** @test */
  public function an_correct_data_structure_should_be_returned_when_an_admin_logs_in()
  {
    $this->mobile = config('test_coolcode.mobile');
    $this->password = config('test_coolcode.password');

    $this->up();

    $response = $this->http->post('/api/auth/login/mobile', [
      'form_params' => [
        'mobile'   => $this->mobile,
        'password' => $this->password,
      ],
    ]);

    $this->assertJsonStructure([
      'token', 'token_ttl', 'id', 'is_admin', 'menu',
      'user_info' => [
        'avatar_url',
        'day_of_birth',
        'gender',
        'month_of_birth',
        'nickname',
        'year_of_birth',
      ],
    ], $this->getContents($response));
  }

  /** @test */
  public function an_admin_which_does_not_exist_can_not_log_in()
  {
    $this->up();
    $code = $this->http->post('/api/auth/login/mobile', [
      'form_params' => [
        'mobile'   => $this->mobile,
        'password' => $this->password,
      ],
    ])->getStatusCode();
    $this->assertEquals(HttpResponse::HTTP_UNPROCESSABLE_ENTITY, $code);
  }

}